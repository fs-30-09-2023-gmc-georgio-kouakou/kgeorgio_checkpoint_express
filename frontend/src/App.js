import logo from './logo.svg';
import './App.css';
import NavBarCustom from './Components/NavBarCustom';
import { Route, Routes } from 'react-router-dom';
import Home from './Components/Home';
import Services from './Components/Services';
import Contact from './Components/Contact';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <NavBarCustom />

        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/services" element={<Services />} />
          <Route path="/contact" element={<Contact />} />
        </Routes>
      </header>
    </div>
  );
}

export default App;
