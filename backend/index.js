const express = require('express');
const app = express();
app.use(express.json());

const getLogginFoHours = function (req, res, next) {
    if(Date.getHours()>17){
        res.send(404);
    } else if(Date.getHours()<9){
        res.send(404);
    }

    next();
}
app.use(getLogginFoHours());
app.listen(5000);